/*
    Smala / Ecouter chuchoter les murs / Marseille / 6 sept. 2010
    simulation de la variation des infos GPS pour le calcul des zones
    echelle 5 pixel = 1 m
      
    Copyright (C) <2010>  <Pierre Commenge, Echelle Inconnue>

    P. Commenge - http://emoc.org
    Echelle Inconnnue - http://echelleinconnue.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


float var = 120; // variation en décimètres
int x, y;
int rayon;

PFont font;
color signalmax = color(225,255,3,255);

String SKETCH_NAME = "sagz";

// controlP5
import controlP5.*;
ControlP5 controlP5;
int myColor = color(0,0,0);
Slider var_slider;




void setup() {
  size(1000,900);
  
  font = loadFont("OhLaLa-10.vlw"); 
  smooth();
  controlP5 = new ControlP5(this);
  controlP5.setColorForeground(0xffaa0000);
  controlP5.setColorBackground(0xff880000);
  controlP5.setColorLabel(0xffffffff);
  controlP5.setColorValue(0xffffffff);
  controlP5.setColorActive(0xffff0000);  
  var_slider = controlP5.addSlider("variation",0,30,12,20,50,100,10);

}

void draw() {
  background(0);
  ellipseMode(CENTER);
  textFont(font, 10);
  stroke(255,250,250,255);fill(255,250,250,255);
  
  var = var_slider.value() * 10;
  text("simulation zones gps / sept. 2010 / variation : +/- " + var / 10 + " m", 15, 30);
  
  // zone de 15m de rayon avec variation de 12m
  x = 450; y = 200; rayon = 150;
  drawZones(x, y, var, rayon);
  drawCenter(x, y, var);
  drawCircles(x, y, 5, rayon);
  drawZoneSure(x, y, var, rayon);
  text("zone de 15m de rayon", x - rayon/2 - var/2 - 10, y - rayon/2 - var/2 - 10);
  
  // zone de 20m de rayon avec variation de 12m
  x = 800; y = 200; rayon = 200;
  drawZones(x, y, var, rayon);
  drawCenter(x, y, var);
  drawCircles(x, y, 5, rayon);
  drawZoneSure(x, y, var, rayon);
  text("zone de 20m de rayon", x - rayon/2 - var/2 - 10, y - rayon/2 - var/2 - 10);
  
  // zone de 30m de rayon avec variation de 12m
  x = 230; y = 620; rayon = 300;
  drawZones(x, y, var, rayon);
  drawCenter(x, y, var);
  drawCircles(x, y, 5, rayon);
  drawZoneSure(x, y, var, rayon);
  text("zone de 30m de rayon", x - rayon/2 - var/2 - 10, y - rayon/2 - var/2 - 10);
  
  // zone de 40m de rayon avec variation de 12m
  x = 710; y = 620; rayon = 400;
  drawZones(x, y, var, rayon);
  drawCenter(x, y, var);
  drawCircles(x, y, 5, rayon);
  drawZoneSure(x, y, var, rayon);
  text("zone de 40m de rayon", x - rayon/2 - var/2, y - rayon/2 - var/2);
  
  loadPixels(); colorMode(HSB, 1);
  for(int i=0; i < width*height; i++) {
    if ((brightness(pixels[i]) > 0.8) && saturation(pixels[i]) == 0) pixels[i] = signalmax;
  }
  updatePixels(); colorMode(RGB, 255);
  
  drawLegende(15, 200);
  
  
  //noLoop();
}

void drawZones(int x, int y, float var, int rayon) {
  for (int i=0; i < 40; i++) {
    fill(255,255,255,11); noStroke();
    strokeWeight(1);
    ellipse(x+random(-var/2,var/2), y+random(-var/2,var/2), rayon, rayon);
  }
}

void drawCenter(int x, int y, float var) {
  fill(255,0,0,255); noStroke();
  ellipse(x, y, 5, 5);
}

void drawCircles (int x, int y, int echelle, int rayon) {
  int j = rayon / 50;
  for (int i=1; i <= j; i++) {
    stroke(255,0,0,150); noFill();
    strokeWeight(1);
    if (i*echelle*10 == rayon) strokeWeight(3);
    ellipse(x, y, i*echelle*10, i*echelle*10);
  }
}

void drawZoneSure(int x, int y, float var, int rayon) {
  stroke(0,255,0,255); noFill(); strokeWeight(1);
  ellipse(x, y, rayon+var, rayon+var);
}

void drawLegende(int x, int y) {
  stroke(255,0,0,255); noFill();
  text("echelle : 25 pixels = 5m", x , y);
  line(x, y+20, x+200, y+20);
  for (int i = 0; i <= 8; i++) {
    line(x + i*25, y+10, x + i*25, y+30);
    text(i*5, x+3 + i*25, y+20-2);
  }
  stroke(255,255,0,255); fill(255,255,0,255);
  text("zone de bonne réception", x , y+70);
  
  stroke(0,255,0,255); fill(0,255,0,255);
  text("zone maximale pour éviter les chevauchements", x , y+90);
}

void keyPressed() {
  if (key == 's') 
    saveFrame(SKETCH_NAME+"_"+year()+month()+day()+hour()+minute()+second()+millis()+".tif");
}


