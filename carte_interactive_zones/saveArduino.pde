void saveArduino() {
  /* 
int zones = 6;              // combien de zones ?
long zoneLat[6]  = {  4799886,  4799863,  4799914, 4799940, 4799949, 4800005}; // coord. x des centres de zone
long zoneLon[6]  = {  -412075,  -411987,  -412041, -412018, -412055, -412005}; // coord. y des centres de zone
long zoneSize[6] = {       30,       30,       20,      15,      15,      40 }; // rayon max pour chaque zone
  */
  String fichier = "";
  int zsz = zones.size();
  fichier += "int zones = " + zsz +";              // combien de zones ?|";
  fichier += "long zoneLat[" + zsz + "]  = {";
  for (int i = 0; i < zsz; i++) {
    Zone zs = (Zone) zones.get(i);
    fichier += "  " + round(zs.ylat * 100000); // arrondir et passer en long
    if (i < zsz - 1) fichier += ",";       // ajouter une virgule   
  }
  fichier += "};  // coord. x des centres de zone|";
  fichier += "long zoneLon[" + zsz + "]  = {";
  for (int i = 0; i < zsz; i++) {
    Zone zs = (Zone) zones.get(i);
    fichier += "   " + round(zs.xlong * 100000); // arrondir et passer en long
    if (i < zsz - 1) fichier += ",";       // ajouter une virgule   
  }
  fichier += "};  // coord. y des centres de zone|";
  fichier += "long zoneSize[" + zsz + "] = {";
  for (int i = 0; i < zsz; i++) {
    Zone zs = (Zone) zones.get(i);
    fichier += "       " + round(zs.rayonm); // arrondir et passer en long
    if (i < zsz - 1) fichier += ",";       // ajouter une virgule   
  }
  fichier += "}; // rayon max pour chaque zone|";

  String[] liste = split(fichier, '|');
  saveStrings("./data/zones_pour_arduino.txt", liste);
}
