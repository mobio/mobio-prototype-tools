void saveKml() {
  String fichier = "";
  fichier += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>|";
  fichier += "<kml xmlns=\"http://earth.google.com/kml/2.2\">|";
  fichier += "<Document> ";
  fichier += "";

  
  for (int i = 0; i < zones.size(); i++) {
    Zone zs = (Zone) zones.get(i);
    fichier += "<Placemark>|";
    fichier += "<name>" + i + "</name>|";
    fichier += "<Point><coordinates>"+ zs.xlong + "," + zs.ylat + "</coordinates></Point>|";
    fichier += "</Placemark>|";
    
  }
  
  fichier += "</Document>|";
  fichier += "</kml>";
  
  String[] liste = split(fichier, '|');
  saveStrings("./data/zones.kml", liste);
}
