/*
    Smala / Ecouter chuchoter les murs / Marseille / 6 sept. 2010
    fond de carte pour le placement interactif des zones GPS
    
    var : variation des infos GPS pour le calcul de la zone de sécurité
      
    Copyright (C) <2010>  <Pierre Commenge, Echelle Inconnue>

    P. Commenge - http://emoc.org
    Echelle Inconnnue - http://echelleinconnue.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



String SKETCH_NAME = "saca";
PImage fond_de_carte;
float metrepix = 180.0 / 320.0; // 180 m sont représentés par 320 pixels
float pixmetre = 320.0 / 180.0; // x pixels = y metres


ArrayList zones = new ArrayList();
int zone_active = -1;
boolean notes_active = true;
PFont font;

float var = 12; // variation en metre
float varpix = var * pixmetre;

// valeurs des coordonnées en degres (approx.)
// en hauteur (latitude) 0°0036 = 784 pixels 
// en largeur (longitude) 0°0036 = 585 pixels

// xref et yref représentent le point lat : 43.3692 long : 5.3532 
float xref = 701;
float yref = 424;

float yrefval = 43.3692; // positif vers le haut (N)
float xrefval = 5.3532; // positif vers la droite (E)

// valeur d'un pixel en long.
float xlongref = 0.0036 / 585;
// valeur d'un pixel en lat.
float ylatref = 0.0036 / 784;




void setup() {
  size(967,916);
  frameRate(30);
  smooth();
  font = loadFont("OhLaLa-10.vlw"); 
  fond_de_carte = loadImage("carte_plan_aou.jpg");
  loadZones();
}

void draw() {
  background(200);
  //background(0);
  image(fond_de_carte, 0, 0);
  textFont(font, 10);
  if (zones.size() > 0) {
    for (int i = 0; i < zones.size(); i++) {
      Zone z = (Zone) zones.get(i);
      z.doActions(i);
    }
  } 
  
  if (notes_active) showNotes();
}

void loadZones() {
  String filename = "zones.txt";
  File f = new File(dataPath(filename));
  if (f.exists()) {
    println("le fichier 'zones.txt' existe, il va être chargé");
    String lines[] = loadStrings("./data/zones.txt");
    for (int i=0; i < lines.length; i++) {
      println(lines[i]);
      String[] val = split(lines[i], ';');
      float _x, _y, _rayon;
      _x = Float.parseFloat(val[0]);
      _y = Float.parseFloat(val[1]);
      _rayon = Float.parseFloat(val[2]);
      println(_x+", "+_y+", "+_rayon);
      zones.add( new Zone(_x, _y, _rayon, metrepix, false));
    }
  } else {
    println("pas de fichier 'zones.txt' à charger");
  }
}

void loadFile() {
  if (zones.size() > 0) {
    for (int i = zones.size()-1; i >= 0; i--) {
      zones.remove(i);
    }
  } 
  loadZones();
}

void nouvelleZone() {
  
  // désélectionner toutes les zones
  for (int i = 0; i < zones.size(); i++) {
    Zone z = (Zone) zones.get(i);
    zone_active = -1;
    z.activeState(false);
  }
  
  zones.add( new Zone(mouseX, mouseY, 30*pixmetre, metrepix, true));

}



void switchNotes() {
  notes_active = !notes_active;
}



void showNotes() {
  fill(255,255,0,255); noStroke();
  rect (0, 0, 300, 450);
  String notes = "";
  notes += "\n\n";
  notes += "clic : selectionner une zone\n\n\n";
  notes += "n : nouvelle zone\n\n";
  notes += "e : effacer la zone selectionnée\n\n";
  notes += "i : cacher / montrer les infos de zone\n\n";
  notes += "+ : agrandir la zone\n\n";
  notes += "- : diminuer la zone\n\n";
  notes += "haut, bas, etc : déplacer la zone\n\n\n\n";
  notes += "c : copie d'écran\n\n";
  notes += "s : enregistrer le fichier des valeurs\n\n";
  notes += "l : charger le dernier fichier des valeurs\n\n\n\n";
  notes += "k : exporter en kml\n\n";
  notes += "a : exporter pour arduino\n\n\n\n";
  notes += "en blanc, la zone théorique définie\n\n\n";
  notes += "en jaune, la zone de réception du signal prenant en compte une variation de +/- " + var + "m\n\n\n";
  notes += "en vert, la limite de zone pour éviter le chevauchement\n\n\n\n\n";
  notes += "'espace' pour cacher/montrer les touches\n\n\n\n";

  
  fill(0, 0, 0, 255); stroke(0, 0, 0, 255);
  text(notes, 10, 5, 290, 430);
}



void mousePressed() {
  if (zones.size() > 0) {
    
    // désélectionner toutes les zones
    for (int i = 0; i < zones.size(); i++) {
      Zone z = (Zone) zones.get(i);
      zone_active = -1;
      z.activeState(false);
    }
    
    // sélectionner une zone si la souris est dedans
    for (int i = 0; i < zones.size(); i++) {
      Zone z = (Zone) zones.get(i);
      if (z.testDistance(mouseX, mouseY)) {
        zone_active = i;
        z.activeState(true);
        println("zone active : " + zone_active);
      } 
    }
  }
}



void saveFile() {
  String fichier = "";
  for (int i = 0; i < zones.size(); i++) {
    Zone zs = (Zone) zones.get(i);
    if (i > 0) fichier += " ";
    fichier += zs.x + ";" + zs.y + ";" + zs.rayon; 
  }
  String[] liste = split(fichier, ' ');
  saveStrings("./data/zones.txt", liste);
}





void keyPressed() {
  println(zone_active);
  if (key == 'c') 
{
    saveFrame(SKETCH_NAME+"_"+year()+month()+day()+hour()+minute()+second()+millis()+".tif");
  }
  if (key == 's') {
    saveFile();
  }
  if (key == 'k') {
    saveKml();
  }
  if (key == 'a') {
    saveArduino();
  }
  if (key == 'l') {
    loadFile();
  }
  
  Zone za;
  
  if (key == ' ') {               // nouvelle zone
    switchNotes();
  }
  
  if ((key == 'n') && (zone_active == -1)) {               // nouvelle zone
    nouvelleZone();
  }
  
  if ((key == 'e') && (zone_active != -1)) {              // effacer une zone
    za = (Zone) zones.get(zone_active);
    zones.remove(zone_active);
    zone_active = -1;
  }
  if ((key == 'i') && (zone_active != -1)) {              // effacer une zone
    za = (Zone) zones.get(zone_active);
    za.switchInfo();
  }
  if ((key == '+') && (zone_active != -1)) {              // agrandir une zone
    za = (Zone) zones.get(zone_active);
    za.modifySize(2.0);
  }
  if ((key == '-') && (zone_active != -1)) {              // diminuer une zone
    za = (Zone) zones.get(zone_active);
    za.modifySize(-2.0);
  }
  if ((keyCode == LEFT) && (zone_active != -1)) {         // déplacer une zone
    za = (Zone) zones.get(zone_active);
    za.move(-2, 0);
  }
  if ((keyCode == RIGHT) && (zone_active != -1)) {        // déplacer une zone
    za = (Zone) zones.get(zone_active);
    za.move(2, 0);
  }
  if ((keyCode == UP) && (zone_active != -1)) {           // déplacer une zone
    za = (Zone) zones.get(zone_active);
    za.move(0, -2);
  }
  if ((keyCode == DOWN) && (zone_active != -1)) {         // déplacer une zone
    za = (Zone) zones.get(zone_active);
    za.move(0, 2);
  }
}



