class Zone {

  float x, y, rayon;
  float xlong, ylat, rayonm;
  boolean active;
  boolean info_active;
  float pixmetre;

  Zone(float _x, float _y, float _rayon, float _metrepix, boolean _active) {
    x = _x;
    y = _y;
    rayon = _rayon;
    active = _active;
    info_active = true;
    metrepix = _metrepix;
    updateRealCoordinates();
  }

  void doActions(int id) {
    
    // remplir la zone circulaire
    noStroke(); fill(255,255,255,100);
    ellipse(x, y, rayon*2, rayon*2);
    
    
    // remplir la zone de meilleur signal
    noStroke(); fill(225,255,3,150);
    ellipse(x, y, rayon*2 - varpix*2, rayon*2 - varpix*2);
    
    // tracer la zone de sécurité
    //stroke(255,255,255,255); noFill();
    stroke(0,255,0,255); noFill();
    ellipse(x, y, (rayon*2) + varpix*2, (rayon*2) + varpix*2);
    
    // entourer la zone si elle est active
    if (active) {
      stroke(255,0,0,255);
      noFill();
      ellipse(x, y, rayon*2, rayon*2);
    }
    

    
    if (info_active) {
      drawInfo(id);
    }
  }
  
  void updateRealCoordinates() {
    xlong = xrefval - ( (xref - x) * xlongref);
    ylat = yrefval + ( (yref - y) * ylatref); 
    rayonm = rayon * metrepix;
  }
  
  void drawInfo(int id) {
    
    // indiquer le centre
    fill(255,0,0,255); noStroke();
    ellipse(x, y, 2, 2);
    
    rectMode(CORNERS);
    
    
    
    
    // informations
    String ch;
    float cw;
    //float xlong = xrefval - ( (xref - x) * xlongref);
    //float ylat = yrefval + ( (yref - y) * ylatref); 
    
    ch = "rayon : " + rayonm;
    cw = textWidth(ch);
    fill(0,0,0,255); noStroke();
    rect(x, y - rayon - 40, x+cw, y - rayon - 50);
    fill(255,255,255,255); noStroke();
    text(ch, x, y - rayon - 40);
    
    ch = "lat : " + ylat;
    cw = textWidth(ch);
    fill(0,0,0,255); noStroke();
    rect(x, y - rayon - 25, x+cw, y - rayon - 35);
    fill(255,255,255,255); noStroke();
    text(ch,x, y - rayon - 25);
    
    ch = "long : " + xlong;
    cw = textWidth(ch);
    fill(0,0,0,255); noStroke();
    rect(x, y - rayon - 10, x+cw, y - rayon - 20);
    fill(255,255,255,255); noStroke();
    text(ch, x, y - rayon - 10);
    
    // afficher le numéro d'ordre de zone
    textFont(font, 20);
    fill(255,0,0,255); noStroke();
    text(id, x - 20, y - 20);
    textFont(font, 10); 
  }
    
  boolean testDistance(float mx, float my) {
    if (dist(x, y, mx, my) <= rayon) return true;
    else return false;
  }
  
  void activeState(boolean state) {
    active = state;
  }
  
  void modifySize(float s) {
    rayon += s;
    if (rayon < var) rayon = var;
    updateRealCoordinates();
  }
  
  void move(float mx, float my) {
    x += mx;
    y += my;
    updateRealCoordinates();
  }
  
  void switchInfo() {
    info_active = !info_active;
  }

}

